# Listenport

Get process's listen tcp port:
* Use `netstat` on Linux
* Use `netstat` and `lsof` on MacOS

Tested on Linux and MacOS 12.6.

Linux needs to install `netstat` first:
```bash
$ apt install net-tools
```

## Install

```bash
go get gitlab.com/oyoclass/listenport
```


## Example

```go
import (
    "fmt"
    "gitlab.com/oyoclass/listenport"
)

func main() {
    pid := 12345
    ports, err := listenport.QueryByPid(pid)
    if err != nil {
        panic(err)
    }
    fmt.Println("Listen on ports:", ports)
}
```