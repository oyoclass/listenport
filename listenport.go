package listenport

import (
	"fmt"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
)

func QueryByPid(pid int) ([]int, error) {
	if runtime.GOOS == "darwin" {
		return darwinQueryByPid(pid)
	}
	if runtime.GOOS == "linux" {
		return linuxQueryByPid(pid)
	}
	return nil, fmt.Errorf("unsupported os %s", runtime.GOOS)
}

func darwinQueryByPid(pid int) ([]int, error) {
	out, err := exec.Command("netstat", "-a", "-n", "-W", "-p", "tcp").
		CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("failed to run netstat: %v", err)
	}
	/*
		Active Internet connections (including servers)
		Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)
		tcp4       0      0  192.168.0.22.56057     136.143.191.56.443     ESTABLISHED
		tcp4       0      0  *.38181                *.*                    LISTEN
	*/
	ret := make([]int, 0)

	lines := strings.Split(string(out), "\n")
	ports := make([]string, 0)
	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) == 0 || !strings.HasPrefix(fields[0], "tcp") ||
			len(fields) != 6 ||
			fields[5] != "LISTEN" {
			continue
		}
		addrFields := strings.Split(fields[3], ".")
		if len(addrFields) == 0 {
			continue
		}
		port := addrFields[len(addrFields)-1]
		ports = append(ports, port)
	}
	if len(ports) == 0 {
		return ret, nil
	}
	param := fmt.Sprintf(":%s", strings.Join(ports, ","))
	// now use lsof
	out, err = exec.Command("lsof", "-i", param, "-n", "-P", "-w").
		CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("failed to run lsof: %v", err)
	}
	lines = strings.Split(string(out), "\n")
	/*
		COMMAND     PID   USER   FD   TYPE             DEVICE SIZE/OFF NODE NAME
		redis-ser  1463 bofeng    6u  IPv4 0x7c5c584d380a0527      0t0  TCP *:6379 (LISTEN)
	*/
	portMap := make(map[int]struct{})
	for i, line := range lines {
		if i == 0 {
			// skip header
			continue
		}
		fields := strings.Fields(line)
		if len(fields) != 10 || fields[9] != "(LISTEN)" {
			continue
		}
		pidField, err := strconv.Atoi(fields[1])
		if err != nil {
			return nil, err
		}
		if pidField == pid {
			addr := strings.Split(fields[8], ":")
			portStr := addr[len(addr)-1]
			port, err := strconv.Atoi(portStr)
			if err != nil {
				return nil, err
			}
			portMap[port] = struct{}{}
		}
	}
	for k := range portMap {
		ret = append(ret, k)
	}
	return ret, nil
}

func linuxQueryByPid(pid int) ([]int, error) {
	// need to install `apt install net-tools`
	out, err := exec.Command("netstat", "-lntp").
		CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("failed to run netstat: %v", err)
	}
	/*
		(Not all processes could be identified, non-owned process info
		will not be shown, you would have to be root to see it all.)
		Active Internet connections (only servers)
		Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
		tcp        0      0 127.0.0.1:6061          0.0.0.0:*               LISTEN      -
		tcp        0      0 0.0.0.0:8181            0.0.0.0:*               LISTEN      27489/python
	*/
	ret := make([]int, 0)

	lines := strings.Split(string(out), "\n")
	portMap := make(map[int]struct{})
	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) == 0 || !strings.HasPrefix(fields[0], "tcp") ||
			len(fields) != 7 ||
			fields[5] != "LISTEN" {
			continue
		}
		proc := strings.Split(fields[6], "/")
		if len(proc) < 2 {
			continue
		}
		pidStr := proc[0]
		pidNum, err := strconv.Atoi(pidStr)
		if err != nil || pidNum != pid {
			continue
		}
		addrFields := strings.Split(fields[3], ":")
		if len(addrFields) < 2 {
			continue
		}
		portStr := addrFields[len(addrFields)-1]
		port, err := strconv.Atoi(portStr)
		if err != nil {
			return nil, err
		}
		portMap[port] = struct{}{}
	}
	if len(portMap) == 0 {
		return ret, nil
	}
	for k := range portMap {
		ret = append(ret, k)
	}
	return ret, nil
}
