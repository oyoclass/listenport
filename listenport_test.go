package listenport

import (
	"fmt"
	"net/http"
	"os"
	"testing"
)

func TestQueryByPid(t *testing.T) {
	port := 8090
	go func() {
		portStr := fmt.Sprintf(":%d", port)
		http.ListenAndServe(portStr, nil)
	}()

	pid := os.Getpid()
	ports, err := QueryByPid(pid)
	if err != nil {
		t.Fatal(err)
	}
	if len(ports) != 1 || ports[0] != port {
		t.Fatalf("failed to find port %d. returned %v", port, ports)
	}
}
